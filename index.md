Safetensors pour C++
----

Cette bibliothèque permet de sauvegarder et importer des tenseurs dans des fichiers.
Chaque tenseur possède un nom unique, un type numérique et une forme (une liste de longueurs).

![format](docs/safetensors-format.svg)

# Types
Les types de données possibles sont

| Nom | type C++ | longueur (octet) |
| --: | :--: | :--------------: |
| BOOL | booléen | 1 |
| U8  | unsigned char | 1 |
| I8 | char | 1 |
| U16 | unsigned short | 2 |
| I16 | short | 2 |
| U32 | unsigned int | 4 |
| I32 | int | 4 |
| U64 | unsigned long | 8 |
| I64 | long | 8 |
| F16 |  | 2 |
| BF16 |  | 2 |
| F32 | flottant simple | 4 |
| F64 | flottant double | 8 |

# Chargement et sauvegarde
```cpp
safetensors::safetensors st;
st.charger("data.safetensors");
st.ajout("weight", {safetensors::F32, {4, 3, 16, 16}}, weight_ptr);
auto tenseur = st["weight"];
st.sauvegarder("data.safetensors");
```

# Intégration PyTorch
L’intégration PyTorch permet de sauvegarder et d’importer directement les `Tensor` et les `Modules`.
Pour vérifier la présence de l’intégration PyTorch, demandez le composant _pytorch_, et _cuda_ pour l’écriture/lecteur directe sur cartes CUDA: `find_package(SafeTensors COMPONENTS pytorch)`.

```cpp
// Initialise un Tensor vierge
auto tensor = safetensors::pytorch::charger(st["weight"] torch::kCPU);
// Réécrit les valeurs mais la taille doit être identique.
st["weight"] >> tensor;
```
```cpp
st >> module; # Importe tous les poids d’un torch::nn::Module
st << module; # Sauvegarde les poids d’un torch::nn::Module
```
