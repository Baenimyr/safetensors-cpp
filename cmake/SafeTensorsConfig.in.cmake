@PACKAGE_INIT@

if(NOT TARGET safetensors)
include("${CMAKE_CURRENT_LIST_DIR}/SafeTensorsTargets.cmake")

set(SafeTensors_pytorch_FOUND @WITH_PYTORCH@)
set(SafeTensors_cuda_FOUND @WITH_CUDA@)

endif()
check_required_components(SafeTensors)
