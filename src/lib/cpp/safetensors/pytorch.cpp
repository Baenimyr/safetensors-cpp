#include "safetensors/pytorch.hpp"
#include "safetensors/safetensors.hpp"

#include <ATen/core/TensorBody.h>
#include <ATen/ops/empty.h>
#include <c10/core/GradMode.h>
#include <c10/core/MemoryFormat.h>
#include <c10/core/TensorOptions.h>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <memory>
#include <span>
#include <stdexcept>
#include <torch/nn/module.h>
#include <vector>

#if safetensors_CUDA
#include <cuda_runtime.h>
#include <torch/cuda.h>
#endif

using namespace std::string_literals;

namespace safetensors::pytorch {

#define PYTORCH_TYPES(_) \
	_(dtype::BOOL, at::kBool) \
	_(dtype::U8, at::kByte) \
	_(dtype::I8, at::kChar) \
	_(dtype::I16, at::kShort) \
	_(dtype::I32, at::kInt) \
	_(dtype::I64, at::kLong) \
	_(dtype::F16, at::kHalf) \
	_(dtype::BF16, at::kBFloat16) \
	_(dtype::F32, at::kFloat) \
	_(dtype::F64, at::kDouble)

dtype safetensor_type(c10::ScalarType type) {
	switch(type) {
#define ST_2_PT(ST, PT) \
	case PT: return ST;
		PYTORCH_TYPES(ST_2_PT)
#undef ST_2_PT
	default: throw std::invalid_argument("Incompatible data type");
	}
} //// safetensor_type

c10::ScalarType pytorch_type(dtype type) {
	switch(type) {
#define PT_2_ST(ST, PT) \
	case ST: return PT;
		PYTORCH_TYPES(PT_2_ST)
#undef PT_2_ST
	default: throw std::invalid_argument("Incompatible data type");
	}
} //// pytorch_type

std::vector<std::size_t> safetensor_forme(at::IntArrayRef dimensions) {
	std::vector<std::size_t> tenseur_forme(dimensions.size());
	std::transform(dimensions.begin(), dimensions.end(), tenseur_forme.begin(),
	  [](auto d) { return static_cast<std::size_t>(d); });
	return tenseur_forme;
} //// safetensor_forme

std::vector<std::int64_t> pytorch_forme(std::vector<std::size_t> const & dimensions) {
	std::vector<std::int64_t> tenseur_forme(dimensions.size());
	std::transform(dimensions.begin(), dimensions.end(), tenseur_forme.begin(),
	  [](auto d) { return static_cast<std::int64_t>(d); });
	return tenseur_forme;
}

} // namespace safetensors::pytorch

bool safetensors::pytorch::cuda_available() {
#if safetensors_CUDA
	return torch::cuda::is_available();
#else
	return false;
#endif
}

SAFETENSORS_LOCAL void memcpy(std::span<char const> donnees, at::Tensor tenseur) {
	auto const dev = tenseur.device();
	if(dev.is_cpu()) {
		std::memcpy(tenseur.data_ptr(), donnees.data(), donnees.size());
#if safetensors_CUDA
	} else if(dev.is_cuda()) {
		cudaMemcpy(tenseur.data_ptr(), donnees.data(), donnees.size(),
		  cudaMemcpyKind::cudaMemcpyHostToDevice);
#endif
	} else {
		throw std::invalid_argument("Ne peut pas charger un tenseur sur la mémoire " + dev.str());
	}
}

SAFETENSORS_LOCAL void memcpy(at::Tensor const & tenseur, std::span<char> donnees) {
	auto const dev = tenseur.device();
	if(dev.is_cpu()) {
		std::memcpy(donnees.data(), tenseur.data_ptr(), donnees.size());
#if safetensors_CUDA
	} else if(dev.is_cuda()) {
		cudaMemcpy(donnees.data(), tenseur.data_ptr(), donnees.size(),
		  cudaMemcpyKind::cudaMemcpyDeviceToHost);
#endif
	} else {
		throw std::invalid_argument("Ne peut pas copier un tenseur depuis la mémoire " + dev.str());
	}
} //// memcpy

at::Tensor safetensors::pytorch::charger(TenseurData const & definition, c10::Device dev) {
	if((dev.is_cuda() && !cuda_available()) && !dev.is_cpu()) {
		throw std::runtime_error("Périphérique non supporté : " + dev.str());
	}

	auto tenseur_options = c10::TensorOptions().dtype(pytorch_type(definition.type())).device(dev);
	at::Tensor tenseur =
	  at::empty(pytorch_forme(definition.forme()), tenseur_options, at::MemoryFormat::Contiguous);

	auto data = definition.data();
	memcpy(data, tenseur);
	return tenseur;
} //// charger

void safetensors::pytorch::charger(TenseurData const & data, at::Tensor & tenseur, bool resize) {
	auto const dtype = pytorch_type(data.type());
	auto const shape = pytorch_forme(data.forme());
	if(shape != tenseur.sizes()) {
		if(resize) {
			tenseur.resize_(shape);
		} else {
			throw std::invalid_argument("Le tenseur est incompatible en taille.");
		}
	}

	if(tenseur.is_contiguous() && tenseur.scalar_type() == dtype) {
		auto valeurs = data.data();
		memcpy(valeurs, tenseur);
	} else {
		auto tenseur_tmp = charger(data, tenseur.device());
		tenseur.copy_(tenseur_tmp);
	}
} //// safetensors::pytorch::charger

at::Tensor & safetensors::operator>>(TenseurData const & td, at::Tensor & tenseur) {
	pytorch::charger(td, tenseur, false);
	return tenseur;
} //// safetensors::operator>>

void safetensors::pytorch::sauver(safetensors & st,
  std::string const & nom,
  at::Tensor const & tenseur) {
	Tenseur definition{safetensor_type(tenseur.scalar_type()), safetensor_forme(tenseur.sizes())};
	auto local_data = std::make_unique_for_overwrite<char[]>(definition.bsize());
	memcpy(tenseur.contiguous(c10::MemoryFormat::Contiguous),
	  {local_data.get(), definition.bsize()});

	st.ajout(nom, TenseurData(definition, std::move(local_data)));
} //// sauver

void safetensors::operator<<(safetensors & st, torch::nn::Module const & module) {
	for(auto const & buffer : module.named_buffers(true)) {
		pytorch::sauver(st, buffer.key(), buffer.value());
	}
	for(auto const & param : module.named_parameters(true)) {
		pytorch::sauver(st, param.key(), param.value());
	}
} //// safetensors::operator<<

void safetensors::operator>>(safetensors const & st, torch::nn::Module & module) {
	torch::NoGradGuard ngg;
	for(auto & buffer : module.named_buffers(true)) {
		st[buffer.key()] >> buffer.value();
	}
	for(auto & param : module.named_parameters(true)) {
		st[param.key()] >> param.value();
	}
} //// safetensors::operator>>
