#include "safetensors/safetensors.hpp"

#include <algorithm>
#include <cstddef>
#include <cstring>
#include <filesystem>
#include <format>
#include <fstream>
#include <functional>
#include <ios>
#include <memory>
#include <nlohmann/json.hpp>
#include <numeric>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

namespace safetensors {

std::size_t bsize(dtype type) {
	switch(type) {
	case dtype::BOOL: return 1;
	case dtype::U8:
	case dtype::I8: return 1;
	case dtype::U16:
	case dtype::I16:
	case dtype::F16:
	case dtype::BF16: return 2;
	case dtype::U32:
	case dtype::I32:
	case dtype::F32: return 4;
	case dtype::U64:
	case dtype::I64:
	case dtype::F64: return 8;
	default: return 1;
	}
} //// bsize

dtype dtype_from_string(std::string const & nom) {
#define IF(D) \
	if(nom == #D) { \
		return D; \
	}
	SAFE_TYPE(IF)
#undef IF
	throw std::invalid_argument("Nom invalide : " + nom);
} //// dtype_from_string

std::string to_string(dtype type) {
	switch(type) {
#define DTYPE_STR(x) \
	case x: return #x;
		SAFE_TYPE(DTYPE_STR)
#undef DTYPE_STR
	default: throw std::invalid_argument("Valeur DType invalide.");
	}
} //// to_string

//
//
//
Tenseur::Tenseur(dtype t, std::vector<std::size_t> tailles) : _type(t), _forme(std::move(tailles)) {
}

std::size_t Tenseur::n_elements() const noexcept {
	if(this->empty())
		return 0;
	return std::accumulate(this->_forme.begin(), this->_forme.end(), 1,
	  std::multiplies<std::size_t>{});
} //// Tensor::n_elements()

std::size_t Tenseur::bsize() const noexcept {
	return this->n_elements() * ::safetensors::bsize(this->type());
} //// Tensor::size() const

//
//
//

TenseurData::TenseurData(Tenseur desc, data_t data)
	: Tenseur(std::move(desc)),
	  _data(std::move(data)) {
}

std::span<safetensors::value_t const> TenseurData::data() const {
	return {this->_data.get(), bsize()};
}

std::span<safetensors::value_t> TenseurData::data() {
	return {this->_data.get(), bsize()};
}

//
//
//
/**
 * \brief Type contenant la position de deux extrémités.
 */
using Bornes = std::array<std::size_t, 2>;

safetensors::~safetensors() = default;

bool safetensors::contains(std::string const & nom) const noexcept {
	return this->_tenseurs.contains(nom);
} //// safetensors::contains

std::set<std::string> safetensors::noms() const {
	std::set<std::string> k;
	for(auto const & [n, v] : this->_tenseurs) {
		k.insert(n);
	}
	return k;
} //// safetensors::noms()

TenseurData const & safetensors::operator[](std::string const & nom) const {
	return this->_tenseurs.at(nom);
}

bool safetensors::ajout(std::string nom, TenseurData && tenseur) {
	return this->_tenseurs.emplace(std::move(nom), std::move(tenseur)).second;
} //// safetensors::ajout

bool safetensors::ajout(std::string nom, Tenseur const & format, value_t const * ptr) {
	auto const size = format.bsize();
	auto data = std::make_unique_for_overwrite<value_t[]>(size);
	std::memcpy(data.get(), ptr, size);
	return this->ajout(std::move(nom), TenseurData(format, std::move(data)));
} //// safetensors::ajout

std::string_view constexpr c_dtype{"dtype"};
std::string_view constexpr c_shape{"shape"};
std::string_view constexpr c_offsets{"data_offsets"};
std::string_view constexpr c_metadata{"__metadata__"};

void safetensors::sauvegarder(std::filesystem::path const & fichier) const {
	auto json = nlohmann::basic_json<>::object();
	json[c_metadata] = this->metadata;

	std::size_t offset = 0;
	auto const noms = this->noms();
	/* Données dans l’ordre d’écriture dans le fichier, sans espace de séparation. */
	std::vector<std::span<value_t const>> data;
	data.reserve(noms.size());

	for(auto const & nom : noms) {
		auto const & tenseur_infos = (*this)[nom];
		auto const tenseur_taille = tenseur_infos.bsize();
		json.emplace(nom,
		  nlohmann::basic_json<>::object(
			{{c_dtype, to_string(tenseur_infos.type())}, {c_shape, tenseur_infos.forme()},
			  {c_offsets, std::vector<std::size_t>{offset, offset + tenseur_taille}}}));
		offset += tenseur_taille;
		data.emplace_back(tenseur_infos.data());
	}

	std::ofstream flux(fichier, std::ios_base::binary);
	{
		std::stringstream metadata;
		metadata << json;
		std::uint64_t const prefix_size = metadata.view().size();
#if __BYTE_ORDER != __ORDER_LITTLE_ENDIAN__
#error Conversion depuis BIG_ENDIAN
#endif
		if(auto const & status =
			 flux.write(reinterpret_cast<char const *>(&prefix_size), sizeof(prefix_size));
		   status.fail()) {
			throw std::runtime_error("Erreur écriture du fichier.");
		}
		flux << metadata.rdbuf();
	}
	for(auto s : data) {
#if __BYTE_ORDER != __ORDER_LITTLE_ENDIAN__
#error Conversion depuis BIG_ENDIAN
#else
		if(auto const & status = flux.write(s.data(), s.size()); status.fail()) {
			throw std::runtime_error("Erreur écriture du fichier.");
		}
#endif
	}
} //// safetensors::sauvegarder

void safetensors::charger(std::filesystem::path const & fichier) {
	std::ifstream data(fichier, std::ios_base::binary);
	if(!data.is_open()) {
		throw std::runtime_error("Fichier illisible : " + fichier.generic_string());
	}
	nlohmann::basic_json<> json;

	{
		std::uint64_t prefix_s;
		if(auto const & status = data.read(reinterpret_cast<char *>(&prefix_s), sizeof(prefix_s));
		   status.fail()) {
			throw std::runtime_error("EOF");
		}
#if __BYTE_ORDER != __ORDER_LITTLE_ENDIAN__
#error Conversion vers BIG_ENDIAN
#endif
		if(prefix_s > 0x6400000) {
			throw std::runtime_error("Les données JSON sont trop grandes.");
		}

		std::string prefix_buffer(prefix_s, '\0');
		if(auto const & status = data.read(prefix_buffer.data(), prefix_s); status.fail()) {
			throw std::runtime_error("EOF");
		}
		json = nlohmann::basic_json<>::parse(prefix_buffer);
	}

	// Positions de vecteurs déjà lus
	std::vector<Bornes> positions;
	for(auto const & definition : json.items()) {
		if(definition.key() == c_metadata) {
			if(!definition.value().is_object() ||
			  std::any_of(definition.value().items().begin(), definition.value().items().end(),
				[](auto const & v) { return !v.value().is_string(); })) {
				throw std::runtime_error("Metadata invalides.");
			}
			this->metadata = definition.value();
		} else {
			auto const & tenseur_nom = definition.key();
			auto const & json_infos = definition.value();
			auto offsets_vector = json_infos[c_offsets].template get<std::vector<std::size_t>>();
			if(offsets_vector.size() != 2 || offsets_vector.at(0) > offsets_vector.at(1)) {
				throw std::invalid_argument("La valeur \"offsets\" doit contenir exactement 2 "
											"valeurs dans l’ordre croissant.");
			}
			Bornes position{offsets_vector.at(0), offsets_vector.at(1)};

			if(std::any_of(positions.begin(), positions.end(), [&position](auto const & o) {
				   return std::get<0>(position) <= std::get<1>(o) &&
					 std::get<0>(o) < std::get<1>(position);
			   })) {
				throw std::invalid_argument("Superposition des données de deux tenseurs.");
			}

			Tenseur const tenseur_infos(
			  dtype_from_string(json_infos[c_dtype].template get<std::string>()),
			  json_infos[c_shape].template get<std::vector<std::size_t>>());

			auto const taille_memoire = std::get<1>(position) - std::get<0>(position);
			if(tenseur_infos.bsize() != taille_memoire) {
				throw std::runtime_error(std::format(
				  "La taille des données ne correspond pas à la taille du tenseur : {} != {}",
				  tenseur_infos.bsize(), taille_memoire));
			}

			std::unique_ptr<char[]> data_buffer = std::make_unique<char[]>(tenseur_infos.bsize());
			if(auto const & status = data.read(data_buffer.get(), tenseur_infos.bsize());
			   status.fail()) {
				throw std::runtime_error("EOF");
			}

			this->ajout(tenseur_nom, TenseurData(tenseur_infos, std::move(data_buffer)));
		}
	}
	// Fin du fichier
	assert((data.get(), data.fail()));
} //// safetensors::charger

} // namespace safetensors
