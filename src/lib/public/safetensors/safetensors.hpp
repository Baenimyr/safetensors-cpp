/**
 * \file safetensors.hpp
 * \author Benjamin Le Rohellec
 *
 * \copyright Copyright 2023 <Benjamin LE ROHELLEC>
 * Apache License v2.0
 */
#pragma once

#include "safetensors/api.h"

#include <filesystem>
#include <map>
#include <memory>
#include <set>
#include <span>
#include <string>
#include <vector>

namespace safetensors {

#define SAFE_TYPE(_) \
	_(BOOL) \
	_(U8) \
	_(I8) \
	_(U16) \
	_(I16) \
	_(F16) \
	_(BF16) \
	_(I32) \
	_(U32) \
	_(F32) \
	_(U64) \
	_(I64) \
	_(F64)

enum SAFETENSORS_API dtype {
	/** bool (1 octet) */
	BOOL,
	/** uint8 */
	U8,
	/** uint16 */
	U16,
	/** uint32 */
	U32,
	/** uint64 */
	U64,
	/** int8 */
	I8,
	/** int16 */
	I16,
	/** int32 */
	I32,
	/** int64 */
	I64,
	/** @} */
	/**
	 * \brief Flottant demi-précision
	 * \see https://fr.wikipedia.org/wiki/Format_virgule_flottante_demi-pr%C3%A9cision
	 */
	F16,
	/**
	 * \brief <em>brain floating point</em>
	 * \see https://en.wikipedia.org/wiki/Bfloat16_floating-point_format
	 */
	BF16,
	/**
	 * \brief Flottant simple précision
	 * \see https://fr.wikipedia.org/wiki/IEEE_754#Format_simple_pr%C3%A9cision_(32_bits)
	 */
	F32,
	/**
	 * \brief Flottant double précision
	 * \see https://fr.wikipedia.org/wiki/IEEE_754#Format_double_pr%C3%A9cision_(64_bits)
	 */
	F64
}; //// enum dtype

/** \brief Nombre d’octets pour stocker un élément de ce type. */
SAFETENSORS_API std::size_t bsize(dtype);

/** \brief Nom du type numérique. */
SAFETENSORS_API std::string to_string(dtype);

/**
 * \brief Retrouve le type à partir de son nom.
 * \throw std::invalid_exception si le texte ne correspond pas.
 */
SAFETENSORS_API dtype dtype_from_string(std::string const &);

/**
 * \brief Classe de description d’un Tenseur.
 */
class SAFETENSORS_API Tenseur {
private:
	dtype _type;
	std::vector<std::size_t> _forme;

public:
	Tenseur(dtype, std::vector<std::size_t>);
	Tenseur(Tenseur &&) noexcept = default;
	Tenseur(Tenseur const &) = default;

	Tenseur & operator=(Tenseur &&) = default;
	Tenseur & operator=(Tenseur const &) = default;

	auto type() const noexcept {
		return this->_type;
	}

	auto const & forme() const noexcept {
		return this->_forme;
	}

	bool empty() const noexcept {
		return this->_forme.empty();
	}

	/** \brief Nombre de dimensions du tenseur. */
	std::size_t dims() const noexcept;
	/** \brief Taille de la dimension i. */
	std::size_t operator[](std::size_t) const;

	/** \brief Nombre d’éléments dans le tenseur. */
	std::size_t n_elements() const noexcept;

	/** \brief Taille en octets. */
	std::size_t bsize() const noexcept;
}; //// Tenseur

/**
 * \brief Description d’un Tenseur et données.
 *
 * Cette classe entrepose les données du Tenseur.
 */
class SAFETENSORS_API TenseurData : public Tenseur {
public:
	using value_t = char;
	using data_t = std::unique_ptr<value_t[]>;

private:
	data_t _data;

public:
	TenseurData(Tenseur, data_t);
	TenseurData(TenseurData &&) noexcept = default;
	TenseurData(TenseurData const &) = delete;
	~TenseurData() = default;

	/**
	 * \brief Retourne un pointeur vers les données non modifiables du tenseur.
	 */
	std::span<value_t const> data() const;

	/**
	 * \brief Retourne un pointeur vers les données modifiables du tenseur.
	 * \warning Fonction dangereuse, à éviter.
	 */
	std::span<value_t> data();
}; //// TenseurData

/**
 * \brief Entrepôt de plusieurs Tenseurs.
 *
 * Cette classe sauvegarde les descriptions des tenseurs \b et les données.
 * Il n’est pas possible de modifier les caractéristiques d’un tenseur après son enregistrement mais les données restent accessibles.
 */
class SAFETENSORS_API safetensors {
public:
	using value_t = char;
	std::map<std::string, std::string> metadata;

private:
	std::map<std::string, TenseurData> _tenseurs;

public:
	safetensors() = default;
	safetensors(safetensors &&) noexcept = default;
	safetensors(safetensors const &) = delete;
	~safetensors();

public:
	bool empty() const noexcept;
	/** \brief Nombre de tenseurs enregistrés. */
	std::size_t size() const noexcept;
	std::size_t max_size() const noexcept;

	/** \brief Vérifie la disponibilité d’un tenseur. */
	bool contains(std::string const &) const noexcept;

	/** \brief Noms des tenseurs. */
	std::set<std::string> noms() const;

	/**
	 * \brief Accès à une définition de tenseur par son nom.
	 * \throw std::invalid_argument si le tenseur n’existe pas.
	 */
	TenseurData const & operator[](std::string const &) const;

public:
	/**
     * \brief Enregistre un nouveau Tenseur dans la mémoire locale.
     *
     * \param ptr pointeur vers le premier élément du Tenseur.
     * La mémoire doit être continue et de longueur \ref Tenseur::bsize().
     */
	bool ajout(std::string nom, Tenseur const & format, value_t const * ptr);

	bool ajout(std::string nom, TenseurData &&);

	/**
	 * \brief Sauvegarde les valeurs dans un fichier.
	 *
	 * Ce fichier est ouvert en mode binaire pour éviter toute tentative de conversion des données entre formats (ex: \\n -> \\r\\n).
	 */
	void sauvegarder(std::filesystem::path const &) const;

	/**
	 * \brief Charge les tenseurs depuis un fichier.
	 *
	 * Cette fonction est protégées des attaques décrite dans https://github.com/huggingface/safetensors/tree/main/attacks#safetensors-abuse-attempts
	 */
	void charger(std::filesystem::path const &);
}; //// safetensors

} // namespace safetensors

//
//
//
inline std::size_t safetensors::Tenseur::dims() const noexcept {
	return this->_forme.size();
}

inline std::size_t safetensors::Tenseur::operator[](std::size_t i) const {
	return this->_forme.at(i);
}

inline bool safetensors::safetensors::empty() const noexcept {
	return this->_tenseurs.empty();
}

inline std::size_t safetensors::safetensors::size() const noexcept {
	return this->_tenseurs.size();
}

inline std::size_t safetensors::safetensors::max_size() const noexcept {
	return this->_tenseurs.max_size();
}
