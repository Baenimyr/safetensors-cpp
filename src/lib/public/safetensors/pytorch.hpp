/**
 * \file pytorch.hpp
 * \author Benjamin Le Rohellec
 *
 * \copyright Copyright 2023 <Benjamin LE ROHELLEC>
 * Apache License v2.0
 */
#pragma once
#include "safetensors.hpp"
#include "safetensors/api.h"

#include <c10/core/Device.h>
#include <c10/core/ScalarType.h>
#include <string>

namespace at {
class Tensor;
}

namespace torch::nn {
class Module;
}

/**
 * \brief Interface PyTorch.
 */
namespace safetensors::pytorch {

/**
 * \brief Convertit un type PyTorch vers un type SafeTensor.
 *
 * Tous les types numériques sont compatibles.
 * Les types complexes n’existent pas dans SafeTensor.
 * \throw std::invalid_argument en cas d’incompatibilité.
 */
SAFETENSORS_API dtype safetensor_type(c10::ScalarType type);

/**
 * \brief Convertit un type SafeTensor vers un type PyTorch.
 *
 * Tous les types n’existe pas dans PyTorch.
 * \throw std::invalid_argument en cas d’incompatibilité.
 */
SAFETENSORS_API c10::ScalarType pytorch_type(dtype type);

[[gnu::const]]
inline constexpr bool cpu_available() {
	return true;
}

[[gnu::const]]
/** \return \c true si la bibliothèque sait lire et écrire sur les périphériques CUDA. */
SAFETENSORS_API bool cuda_available();

/**
 * \brief Crée un Tensor.
 *
 * Le Tensor est crée en utilisant le type, les dimensions et les données enregistrées dans le SafeTensor.
 * Le \e Tensor est propriétaire des données.
 *
 * \warning S’il n’existe pas de correspondance entre le type de donnée du safetensors::Tensor et les types compatibles PyTorch, une erreur est déclenchée.
 *
 * \param peripherique est l’emplacement mémoire du Tensor
 */
SAFETENSORS_API at::Tensor charger(TenseurData const &, c10::Device peripherique = c10::kCPU);

/**
 * \brief Charge la valeur dans un Tensor existant.
 *
 * L’écriture des données est directe si le type est le même.
 * La conversion entre types, si nécessaire, est gérée par \e at::Tensor::copy_.
 *
 * \param resize redimensionne le \e at::Tensor si nécessaire.
 * \throw std::invalid_argument si la taille du \e at::Tensor est incompatible.
 * \see https://pytorch.org/docs/stable/generated/torch.Tensor.resize_.html
 */
SAFETENSORS_API void charger(TenseurData const &, at::Tensor &, bool resize = false);

/**
 * \brief Sauvegarde le Tensor.
 *
 * Le Tensor doit être sur un périphérique compatible et être continu.
 * Les données sont recopiées dans la mémoire du \e safetensors.
 *
 * \warning S’il n’existe pas de correspondance entre le type de donnée du at::Tensor et les types compatibles SafeTensor, une erreur est déclenchée.
 *
 * \param nom du vecteur
 */
SAFETENSORS_API void sauver(safetensors &, std::string const & nom, at::Tensor const &);

} // namespace safetensors::pytorch

namespace safetensors {

SAFETENSORS_API void operator<<(safetensors &, torch::nn::Module const &);
/** \see safetensors::pytorch::charger(TenseurData const &, at::Tensor &, bool) */
SAFETENSORS_API at::Tensor & operator>>(TenseurData const &, at::Tensor &);
SAFETENSORS_API void operator>>(safetensors const &, torch::nn::Module &);

} // namespace safetensors
