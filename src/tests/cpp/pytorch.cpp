#include "safetensors/pytorch.hpp"

#include "safetensors/safetensors.hpp"

#include <algorithm>
#include <c10/core/Device.h>
#include <c10/core/DeviceType.h>
#include <c10/core/ScalarType.h>
#include <c10/core/TensorOptions.h>
#include <cstdlib>
#include <gtest/gtest.h>
#include <limits>
#include <stdexcept>
#include <torch/cuda.h>
#include <torch/nn/init.h>
#include <torch/nn/module.h>
#include <torch/nn/modules/activation.h>
#include <torch/nn/modules/conv.h>
#include <torch/nn/options/conv.h>
#include <torch/types.h>

TEST(PyTorch, chargement) {
	safetensors::safetensors tensors;
	safetensors::Tenseur details{safetensors::dtype::I16, {5, 17, 23}};
	auto data = std::vector<short>(details.n_elements());
	std::generate(data.begin(), data.end(),
	  []() { return std::rand() % std::numeric_limits<short>::max(); });

	tensors.ajout("test", details, reinterpret_cast<char const *>(data.data()));

	{
		auto tenseur = safetensors::pytorch::charger(tensors["test"], c10::kCPU);
		EXPECT_TRUE(tenseur.device().is_cpu());
		EXPECT_EQ(c10::kShort, tenseur.scalar_type());
		EXPECT_EQ(details.dims(), tenseur.dim());

		auto tenseur_cpu_flat = tenseur.flatten();
		ASSERT_TRUE(tenseur_cpu_flat.is_cpu());
		auto tenseur_cpu_access = tenseur_cpu_flat.accessor<short, 1>();
		for(std::size_t i = 0; i < tenseur_cpu_flat.size(0); ++i) {
			EXPECT_EQ(data[i], tenseur_cpu_access[i]);
		}
	}
}

#if safetensors_CUDA
TEST(PyTorch, chargement_cuda) {
	ASSERT_TRUE(torch::cuda::is_available());
	ASSERT_TRUE(safetensors::pytorch::cuda_available());
	safetensors::safetensors tensors;
	safetensors::Tenseur details{safetensors::dtype::I16, {5, 17, 23}};
	auto data = std::vector<short>(details.n_elements());
	std::generate(data.begin(), data.end(),
	  []() { return std::rand() % std::numeric_limits<short>::max(); });

	tensors.ajout("test", details, reinterpret_cast<char const *>(data.data()));

	{
		auto tenseur = safetensors::pytorch::charger(tensors["test"], c10::kCUDA);
		EXPECT_TRUE(tenseur.device().is_cuda());
		EXPECT_EQ(c10::kShort, tenseur.scalar_type());
		EXPECT_EQ(details.dims(), tenseur.dim());

		auto tenseur_cpu_flat = tenseur.cpu().flatten();
		ASSERT_TRUE(tenseur_cpu_flat.is_cpu());
		auto tenseur_cpu_access = tenseur_cpu_flat.accessor<short, 1>();
		for(std::size_t i = 0; i < tenseur_cpu_flat.size(0); ++i) {
			EXPECT_EQ(data[i], tenseur_cpu_access[i]);
		}
	}
}
#endif

TEST(PyTorch, sauvegarde) {
	safetensors::safetensors st;

	auto tenseur =
	  torch::empty({7, 13, 11}, c10::TensorOptions().dtype(at::kFloat).device(c10::kCPU));
	torch::nn::init::kaiming_normal_(tenseur);

	safetensors::pytorch::sauver(st, "test", tenseur);
	auto const & description = st["test"];
	EXPECT_EQ(tenseur.dim(), description.dims());
	EXPECT_EQ(safetensors::dtype::F32, description.type());

	{
		float const * data = reinterpret_cast<float const *>(description.data().data());
		auto tenseur_cpu_flat = tenseur.cpu().flatten();
		auto tenseur_cpu_access = tenseur_cpu_flat.accessor<float, 1>();
		for(std::size_t i = 0; i < tenseur_cpu_flat.size(0); ++i) {
			ASSERT_EQ(data[i], tenseur_cpu_access[i]);
		}
	}
}

struct TestModule : public torch::nn::Module {
	torch::nn::Conv2d conv_0;
	torch::nn::ReLU relu_1;
	torch::nn::Conv2d conv_2;

	TestModule();
};

TestModule::TestModule()
	: conv_0(register_module("conv_0",
		torch::nn::Conv2d(torch::nn::Conv2dOptions(3, 8, {3, 3}).bias(true)))),
	  relu_1(register_module("relu_1", torch::nn::ReLU())),
	  conv_2(register_module("conv_2",
		torch::nn::Conv2d(torch::nn::Conv2dOptions(8, 8, {3, 3}).bias(false)))) {
}

TEST(PyTorch, module_sauv) {
	TestModule const module;
	safetensors::safetensors st;

	st << module;
	auto noms = st.noms();
	EXPECT_EQ(3, noms.size());
	EXPECT_TRUE(noms.contains("conv_0.weight"));
	EXPECT_TRUE(noms.contains("conv_0.bias"));
	EXPECT_TRUE(noms.contains("conv_2.weight"));
	EXPECT_FALSE(noms.contains("conv_2.bias"));
}

TEST(PyTorch, module_reload) {
	TestModule const module;
	TestModule module_clone;
	torch::nn::init::kaiming_normal_(module.conv_0->weight);
	torch::nn::init::uniform_(module.conv_0->bias);
	torch::nn::init::kaiming_normal_(module.conv_2->weight);

	safetensors::safetensors st;

	st << module;
	st >> module_clone;

	EXPECT_TRUE(module.conv_0->weight.eq(module_clone.conv_0->weight).all().item<bool>());
	EXPECT_TRUE(module.conv_0->bias.eq(module_clone.conv_0->bias).all().item<bool>());
	EXPECT_TRUE(module.conv_2->weight.eq(module_clone.conv_2->weight).all().item<bool>());

	at::Tensor mauvaise_taille = torch::empty({8, 5, 5});
	EXPECT_THROW((st["conv_0.weight"] >> mauvaise_taille), std::invalid_argument);
}
