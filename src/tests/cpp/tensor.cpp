#include "safetensors/safetensors.hpp"

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <gtest/gtest.h>
#include <memory>
#include <numbers>
#include <vector>

TEST(SafeTensors, vide) {
	safetensors::Tenseur const tenseur_vide{safetensors::dtype::I16, {}};
	EXPECT_EQ(tenseur_vide.n_elements(), 0);
	EXPECT_EQ(tenseur_vide.bsize(), 0);
	EXPECT_EQ(tenseur_vide.dims(), 0);

	safetensors::Tenseur const tenseur_zero{safetensors::dtype::F64, {17, 16, 0, 3}};
	EXPECT_EQ(tenseur_zero.n_elements(), 0);
	EXPECT_EQ(tenseur_zero.bsize(), 0);
	EXPECT_EQ(tenseur_zero.dims(), 4);
	EXPECT_EQ(tenseur_zero[0], 17);
}

TEST(SafeTensors, tailles) {
	safetensors::Tenseur const tenseur_u8{safetensors::dtype::U8, {5, 7}};
	EXPECT_EQ(tenseur_u8.n_elements(), 5 * 7);
	EXPECT_EQ(tenseur_u8.bsize(), 5 * 7 * sizeof(uint8_t));
	EXPECT_EQ(tenseur_u8[0], 5);
	EXPECT_EQ(tenseur_u8[1], 7);

	safetensors::Tenseur const tenseur_f32{safetensors::dtype::F32, {17, 2}};
	EXPECT_EQ(tenseur_f32.n_elements(), 17 * 2);
	EXPECT_EQ(tenseur_f32.bsize(), 17 * 2 * sizeof(float));

	safetensors::Tenseur const tenseur_f64{safetensors::dtype::F64, {11, 2}};
	EXPECT_EQ(tenseur_f64.n_elements(), 11 * 2);
	EXPECT_EQ(tenseur_f64.bsize(), 11 * 2 * sizeof(double));
}

TEST(SafeTensors, init) {
	safetensors::safetensors tensors;
	EXPECT_TRUE(tensors.empty());
	EXPECT_EQ(0, tensors.size());
	EXPECT_LT(0, tensors.max_size());

	auto const noms = tensors.noms();
	EXPECT_TRUE(tensors.empty());
}

TEST(SafeTensors, ajout) {
	safetensors::safetensors tensors;

	std::vector<std::size_t> const dims{3, 128, 19};
	auto const fausses_donnees = std::make_unique<float[]>(3ULL * 128 * 19);
	tensors.ajout("Tensor_1", {safetensors::dtype::F32, dims},
	  reinterpret_cast<safetensors::safetensors::value_t *>(fausses_donnees.get()));

	EXPECT_FALSE(tensors.empty());
	EXPECT_EQ(1, tensors.size());

	auto const k = tensors.noms();
	EXPECT_TRUE(std::find(k.begin(), k.end(), "Tensor_1") != k.end());
	auto const & T = tensors["Tensor_1"];
	EXPECT_EQ(3ULL * 128 * 19, T.n_elements());
	EXPECT_EQ(3ULL * 128 * 19 * sizeof(float), T.bsize());

	auto data = T.data();
	EXPECT_TRUE(std::strncmp(reinterpret_cast<char const *>(fausses_donnees.get()), data.data(),
				  T.bsize()) == 0);
}

TEST(SafeTensors, sauvegarde) {
	using type = float;
	safetensors::Tenseur description(safetensors::dtype::U16, {8, 10, 1});
	std::vector<type> data(description.n_elements());
	std::span<std::uint64_t> data_mask{reinterpret_cast<std::uint64_t *>(data.data()),
	  data.size() / (sizeof(std::uint64_t) / sizeof(type))};

	std::generate(data_mask.begin(), data_mask.end(),
	  []() -> std::uint64_t { return 0x89ffdd0065432100; });

	safetensors::safetensors tenseurs;
	tenseurs.ajout("mask_1", description, reinterpret_cast<char const *>(data.data()));

	tenseurs.sauvegarder("test.safetensors");
}

TEST(SafeTensors, lecture) {
	safetensors::safetensors valeurs;
	ASSERT_NO_THROW(valeurs.charger(std::filesystem::path(__FILE__).parent_path().parent_path() /
	  "ressources" / "test-lecture.safetensors"));

	auto const & description = valeurs["f"];
	EXPECT_EQ(3, description.dims());
	EXPECT_EQ(3, description[0]);
	EXPECT_EQ(8, description[1]);
	EXPECT_EQ(8, description[2]);
	EXPECT_EQ(safetensors::dtype::F32, description.type());

	{
		std::span<safetensors::safetensors::value_t const> const data = description.data();
		EXPECT_EQ(data.size(), 3 * 8 * 8 * sizeof(float));

		std::span<std::uint64_t const> data_cast{
		  reinterpret_cast<std::uint64_t const *>(data.data()),
		  data.size() / (sizeof(std::uint64_t) / sizeof(safetensors::safetensors::value_t))};
		auto correct = std::all_of(data_cast.begin(), data_cast.end(),
		  [](auto const & v) { return v == 0xab83e93710384aad; });
		EXPECT_TRUE(correct);
	}
}
